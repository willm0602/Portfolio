// Module to contain all custom components
import Layout from "./Layout.astro";
import TopNav from "./TopNav";

export { Layout, TopNav };
