import type { Link } from "./Link";

const links: Link[] = [
  {
    name: "Home",
    path: "/",
  },
  {
    name: "Projects",
    path: "/project",
  },
  {
    name: "Blog",
    path: "/blog",
  },
];

export default links;
