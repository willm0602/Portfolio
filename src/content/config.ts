import { z, defineCollection } from "astro:content";

const blogCollection = defineCollection({
  type: "content", // v2.5.0 and later
  schema: z.object({
    title: z.string(),
    description: z.string(),
    isDraft: z.boolean().optional(),
  }),
});

const projCollection = defineCollection({
  type: "data",
  schema: z.object({
    title: z.string(),
    description: z.string(),
    embedLink: z.string().optional(),
    // we use string here so we can include units
    embedHeight: z.string().optional(),
    embedWidth: z.string().optional(),
    github: z.string(),
    tags: z.array(z.string()).optional(),
    sortIndex: z.number(),
  }),
});

export const collections = {
  blogs: blogCollection,
  projects: projCollection,
};
