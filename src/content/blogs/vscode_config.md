---
title: My VSCode Config
description: Settings I use for VSCode
date: 9/12/2024
isDraft: true
---

![""](./assets/vscode_config/overview.png)

According to the [2024 Stack Overflow Devloper Survey](https://survey.stackoverflow.co/2024/technology#most-popular-technologies-new-collab-tools),
over seventy-percent of software engineers use VSCode.

![Image of Stack Overflow 2024 Developer Survey](./assets/vscode_config/stack_overflow_survey.png)

One of my favorite aspects of this editor is the customizability that
it has. You can do pretty much anything with it, [including adding pets inside](https://marketplace.visualstudio.com/items?itemName=tonybaloney.vscode-pets).

![Image of pets inside VSCode editor](./assets/vscode_config/pets.png)

Personally, I've been using it as my primary editor for four-years.
I wanted to write this article to talk about my configuration for
VSCode including the extensions I use, my keyboard shortcuts and the
other settings I use.

---

## Extensions

As someone who has a wide range of projects that I work on, I've been
utilizing VSCodes [new profiles feature](https://code.visualstudio.com/docs/editor/profiles)
to use specific extensions based on what I am working on. There are
some extensions though that I use for all profiles

### [File Utils](https://marketplace.visualstudio.com/items?itemName=sleistner.vscode-fileutils)

The file utils extension is an extension to modify files inside VSCode.
It gives you the ability to

- rename files
- delete files
- duplicate files
- move files
- create files/directories

which is.... all things you can do in VSCode on its own. This however
makes it so that you can perform these actions without needing to use
the sidebar with the file explorer. With the file utils extension, you
can perform these actions using the VSCode fuzzy finder. It lets you
quickly perform actions on the current file without needing to use
your mouse.

### [Bookmarks](https://marketplace.visualstudio.com/items?itemName=alefragnani.Bookmarks)

The bookmarks extension allows you to bookmark places in the codebase
that you can access later. This might seem trivial with the use of the
symbol searcher (accessible with command+t) but there are still times
I've found this to be really useful.

1. Working with larger files that don't use symbols (e.g. HTML, CSS, Markdown)
2. Working with larger methods to remember a spot in a method in particular
3. Working with methods with unclear names (In these cases, you
   could also make a case that you should just rename the method. In some
   cases though, if the method is used in a lot of places then it may
   not be worth it).

### [Intellicode](https://marketplace.visualstudio.com/items?itemName=VisualStudioExptTeam.vscodeintellicode)

Intellicode is an extension developed my Microsoft to perform autocomplete
for Python, JavaScript and TypeScript files. It feels weird to cut it
off there, but it's as simple as having great autocompletions.

![""](./assets/vscode_config/intellisense-example.png)

### [Paste Image](https://marketplace.visualstudio.com/items?itemName=mushan.vscode-paste-image)

The Paste Image extension is an extension used to be able to instantly
paste any images from the clipboard. I actually just started using this,
but it is really helpful for times when I need to import image assets
into the codebase

### [Intellisense for CSS Class Names](https://marketplace.visualstudio.com/items?itemName=Zignd.html-css-class-completion) / [Tailwind CSS Intellisense](https://marketplace.visualstudio.com/items?itemName=bradlc.vscode-tailwindcss)

I'm lumping these two extensions together since they fulfill a very
similar purpose of being able to provide autocompelte for CSS classnames
for HTML / web component files such as JSX or Svelte Components.

### [Trailing Spaces](https://marketplace.visualstudio.com/items?itemName=shardulm94.trailing-spaces)

This is a smaller extension used for showing trailing spaces and
adding the ability to quickly remove them. This is a minor quality-of-life
feature but it helps a lot for finding and removing random white-space
from the codebase.

![""](./assets/vscode_config/trailing_spaces.png)

## Keyboard Shortcuts

As someone that uses VSCode on Windows for personal development and
Mac for work, it's important for me to be able to have consistent
shortcuts for both options.

I also don't want to rely on the function keys. Most of the keyboard
shortcuts I've overwritten use function keys in combination with another
key in order to use these shortcuts a lot easier

`Shift+F12` to `Ctrl-Alt-X` loads all references to symbols
this lets me quickly be able to see all the places a symbol is being
used in the codebase. This helps when refactoring code to ensure it's
not going to break any cases where it's currently being used
![''](./assets/vscode_config/find_symbols.png)

`f12` to `Ctrl-Alt-D` goes to the definition of a method. Pretty self
explanatroy but this is helpful for when I need to see exactly how
a method/Class works. This also works when using dependencies which
is especially helpful when working with third-party libraries.

`f2` to `Ctrl-Alt-E` lets you rename a variable. When you update a variable
name with this, it updates the variable name throughout the entire codebase.

`Ctrl+g` to `Ctrl+Shift+;` is a shortcut I added to be able to jump to
a line number. This shortcut I felt was a bit more clear then using `Ctrl+g`
since it follows the pattern the VSCode fuzzy finder has with its line
search prefacing with a colon.

![''](./assets/vscode_config/gotoline.png)

`Alt+~` is one that I added (it actually uses a backtick, but that
couldn't be expressed in markdown for some reason). This one I added to
open a terminal session in the editor workspace in VSCode.

![''](./assets/vscode_config/terminal.png)

I don't normally use this much since I'll tend to use an external terminal
emulator (either Windows Terminal or ITerm2), but this can be helpful
for when I need to do quick file manipulation from the command line that
I can't do with `File Utils` for whatever reason.

## Other VSCode Settings I Use

`files.autoSave: afterDelay`
This allows files to autosave after a delay. I do this mostly cause I
can honestly make mistakes sometimes where I'll forget to save without
it. I do however have a couple of exceptions setup in my settings.

```json
"[less]": {
    "files.autoSave": "off"
},
```

At work, I use LESS CSS which requires a bundler process. From my experience,
bundling LESS can be slow (takes about thirty-seconds). If I have auto-save
on for it, it will start bundling before I'm done with changes, requiring
**another** thirty-seconds before seeing the changes.

Similarly, I've had slow bundling times with JavaScript. Because of this,
I have it setup for JavaScript as well:

```json
"[javascript]": {
    "editor.defaultFormatter": "vscode.typescript-language-features",
    "files.autoSave": "off"
}
```

With JavaScript, you can see that I have an additional setting though,
`editor.defaultFormatter`. This setting is used with the `Prettier` VSCode
Extension to be able to format files. All you need to do to customize
your formatting is to add a `prettierrc.json` file. You can read how to
configure this file [here.](https://prettier.io/docs/en/configuration.html)

There are also some general appearance-based editor settings that I use:

```json
"editor.wordWrap": "on",
"window.density.editorTabHeight": "compact",
"editor.mouseWheelZoom": true,
"editor.lineHeight": 2.4,
```

Going through these step-by-step,
`editor.wordWrap` makes it so if the text would go off-screen, it'll instead
go to the next line.

!["Image showing text overflowing to next line in editor, but all being on one line."](./assets/vscode_config/overflow.png)

As we can see in the above example, the line eventually wraps to the next
line visually. This way you don't need to scroll if the editor goes for too long.

You can also see in the code above that I have two visual red lines. Those were
added with the following:

```json
"editor.rulers": [
    {
      "color": "#ff0000",
      "column": 99,
    },
    {
      "color": "#ff000088",
      "column": 80,
    }
],
```

I tend to have a code style of ensuring lines of code are less than 99 columns
wide and try to stay under 80 columns. Hence, the two columns I have included,
the first one being a "warning" and the second being a hard maximum.

### Tailwind CSS Intellisense Assistance

The Tailwind CSS Intellisense extension that I used only provides autocomplete when
adding the attributes, "class" or "className" to an element. For 90% of the time,
this is ok. But what if we want to make classNames a bit more variable. For example,
I was working on a side-project where I was adding a tabpane component and wanted
to be able to set the class names used for the main content, the tabs and the
active tab specifically, which can't be set with one className attribute.

```js
export default function TabPane(props: TabPaneProps) {
    const {
        tabs,
        className = 'tabs tabs-lifted tabs-boxed w-full',
        id,
        activeTabClassName,
        tabClassName,
    } = props;
    ...
```

I still wanted the intellisense for these class names though, hence why I added
these settings:

```json
"tailwindCSS.classAttributes": [
    "class:list",
    "DEFAULT_CLASS_NAME"
],
"tailwindCSS.emmetCompletions": true,
"tailwindCSS.experimental.classRegex": [
    ".*class$",
    ".*className$",
    ".*ClassName$"
],
```

This allows the autocomplete to be loaded for any of the following attribute names

- the attribute is named "class:list" (mostly used for Svelte)
- the attribute is named "DEFAULT_CLASS_NAME" (which is one I use commonly for a
  default class name).
- the class ends in `class, className` or `ClassName`. This lets me add properties
  to components that I can pass in that should clearly indicate that they are using
  `CSS classes` and the autocomplete will work for them.

![""](./assets/vscode_config/css_intellisense_settings.png)

## Summary

I love VSCode because of how customizable it is. It has a lot of helpful features
out of the gate to the point that I'd argue this level of customization isn't necessary,
but still definitely helpful for optimizing my developer experience. If you have
any recommendations for other configurations you think I should change, please feel
free to reach out and let me know!
