import { getCollection, type CollectionEntry } from "astro:content";
import type { Tag } from "./projectTag";

type Blog = CollectionEntry<"blogs">;
type Project = CollectionEntry<"projects">;

async function getProjectsInOrder(): Promise<Project[]> {
  const projects = await getCollection("projects");
  return projects.sort((a, b) => {
    return a.data.sortIndex - b.data.sortIndex;
  });
}

function getUsedProjectTags(): Promise<Set<string>> {
  return new Promise(async (res, rej) => {
    const projects = await getCollection('projects');
    const usedTags: Set<string> = new Set();
    projects.forEach((proj) => {
      const data = proj.data;
      const tags: string[] = data.tags || [];
      tags.forEach((tag) => {
        usedTags.add(tag);
      })
    });
    res(usedTags);
  })
}

async function getBlogsInOrder(): Promise<Blog[]> {
  const blogs = await getCollection("blogs");
  return blogs
    .sort((a, b) => {
      const firstBlogDate = new Date(a.data.date).getTime();
      const secondBlogDate = new Date(b.data.date).getTime();
      return firstBlogDate - secondBlogDate;
    })
    .filter((blog) => {
      return !blog.data.isDraft;
    });
}

export { getUsedProjectTags, getProjectsInOrder, getBlogsInOrder };
