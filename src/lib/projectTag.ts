type TagType = "Project Type" | "Language" | "Library" | "Other";

export type Tag = {
  name: string;
  type: TagType;
  slug: string;
};

const ProjectTypeTags: Tag[] = [
  {
    name: "Web Dev",
    slug: "webdev",
  },
].map((tag) => ({ ...tag, type: "Project Type" }));

const LanguageTags: Tag[] = [
  {
    name: "JavaScript",
    slug: "js",
  },
  {
    name: "TypeScript",
    slug: "ts",
  },
  {
    name: "Python",
    slug: "py",
  },
  {
    name: "Java",
    slug: "java",
  },
].map((tag) => ({ ...tag, type: "Language" }));

const LibraryTags: Tag[] = [
  {
    name: "React",
    slug: "react",
  },
  {
    name: "Django",
    slug: "django",
  },
  {
    name: "Flask",
    slug: "flask",
  },
  {
    name: "ThreeJS",
    slug: "threejs",
  },
  {
    name: "Express",
    slug: "express",
  },
  {
    name: "Next.js",
    slug: "nextjs",
  },
  {
    name: "Node.js",
    slug: "nodejs",
  },
  {
    name: "Tailwind CSS",
    slug: "tailwind",
  },
  {
    name: "DaisyUI",
    slug: "daisyui",
  },
  {
    name: "Astro",
    slug: "astro",
  },
  {
    name: "Vite",
    slug: "vite",
  },
  {
    name: "Firebase",
    slug: "firebase",
  },
  {
    name: "Svelte",
    slug: "svelte",
  },
].map((tag) => ({ ...tag, type: "Library" }));

const Tags: Tag[] = [...ProjectTypeTags, ...LanguageTags, ...LibraryTags];

function getTagFromSlug(slug: string): Tag | undefined {
  return Tags.find((tag) => tag.slug === slug);
}

function getTagsFromSlugs(slugs: string[] | undefined): Tag[] {
  if (slugs === undefined) {
    return [];
  }
  return slugs
    .map((slug) => getTagFromSlug(slug))
    .filter((tag) => tag !== undefined) as Tag[];
}

export type { TagType };
export default Tags;
export { getTagFromSlug };
export { getTagsFromSlugs };
