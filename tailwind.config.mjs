import daisy from "daisyui";
import typography from "@tailwindcss/typography";

/** @type {import('tailwindcss').Config} */
const config = {
  mode: "jit",
  purge: ["./public/**/*.html", "./src/**/*.{astro,js,jsx,ts,tsx,vue}"],
  theme: {
    extend: {},
  },
  content: ["./src/**/*.{astro,html,svelte,vue,js,ts,jsx,tsx}"],
  plugins: [daisy, typography],
  daisyui: {
    themes: ["dark", "retro"],
    base: true,
    styled: true,
    utils: true,
    prefix: "",
    logs: false,
    themeRoot: ":root",
  },
  darkMode: ["selector", 'data-theme="dark"'],
  safelist: [
    "btn-secondary",
    "btn-accent",
    "bg-blue-800",
    "bg-base-400",
    "text-4xl",
    "badge-neutral",
    "border-red-400",
    "text-red-400",
    "border-yellow-400",
    "text-yellow-400",
    "text-blue-400",
    // 'badge-primary',
    // 'badge-accent',
    // 'badge-secondary',
    // 'badge-info',
    // 'badge-success',
    "border-green-500",
    "text-blue-500",
    "text-green-500",
    "border-blue-500",
    "bg-blue-500",
    "bg-green-500",
    "bg-yellow-700",
    "bg-gray-500",
  ],
};

export default config;
