import tailwind from "./tailwind.config.mjs";

export default {
  plugins: {
    tailwindcss: tailwind,
    autoprefixer: {},
  },
};
