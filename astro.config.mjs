import react from "@astrojs/react";
import icon from "astro-icon";
import { defineConfig } from "astro/config";
import { remarkModifiedTime } from "./remarkModifiedTime.mjs";

// https://astro.build/config
export default defineConfig({
  integrations: [react(), icon()],
  site: "https://willm0602.gitlab.io/Portfolio",
  outDir: "public",
  publicDir: "static",
  markdown: {
    syntaxHighlight: "prism",
    remarkPlugins: [remarkModifiedTime()]
  }
});
